init:
	make build
	make start

build:
	docker-compose build

start:
	docker-compose up -d

stop:
	docker-compose stop

reset:
	docker-compose down -v;
	docker-compose build --no-cache
	make start

destroy:
	docker-compose down -v

database-bash:
	docker-compose exec database /bin/bash

create-database:
	docker-compose exec database mysql
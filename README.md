
# Phalcon Mini ERP



## Summary

- Installation
- Licence & Credits

## Installation

**Prerequisites :**
- Docker
- Docker-compose
- Git
- Make

Clone repository : `git clone git@gitlab.com:auchalet/phalcon-mini-erp.git` for SSH

This project has 2 Makefiles : one for manage Docker containers, and an other for app tasks

In root path, launch containers with : `make init`

`cd src/`

Install assets : `make composer-install`  
Apply migrations : `make migrate`

Enjoy browser access in : `http://localhost:8080`

## Routes available

- `/company/list`
- `/company/view/{id}`
- `/company/create`
- `/company/update/{id}`
- `/company/delete/{id}`

## Tests

Unit tests are configured with Codeception
100% of code is covered by tests

To launch tests suite, run in `src` directory : `make test-all`

## Licence & Credits

This project is licensed under the Apache License, Version 2.0.

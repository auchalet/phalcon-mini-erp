<?php

declare(strict_types=1);

namespace ERP\DTO;

class CompanyDTO
{
    public ?int $id = null;

    public ?string $name = '';

    public float $balance;

    public ?string $country = '';
}

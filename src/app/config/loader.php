<?php

$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->formsDir,
        $config->application->appDir . '/services/',
        $config->application->appDir . '/DTO/',
    ]
);

$loader->registerNamespaces(
    [
        'ERP\Controllers' => $config->application->controllersDir,
        'ERP\Forms'       => $config->application->formsDir,
        'ERP\Models'      => $config->application->modelsDir,
        'ERP\Services'     => $config->application->appDir . '/services/',
        'ERP\Repositories'     => $config->application->appDir . '/repositories/',
    ]
);

$loader->register();

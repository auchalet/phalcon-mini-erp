<?php

use Phalcon\Mvc\Router;

$router = new Router();

$router->setDefaultNamespace('ERP\Controllers');

$router->add(
    '/',
    [
        'controller' => 'company',
        'action' => 'list'
    ]
);

$router->add(
    '/:controller/:action/:params',
    [
        'controller' => 1,
        'action' => 2,
        'params' => 3
    ]
);

return $router;

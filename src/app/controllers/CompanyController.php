<?php

declare(strict_types=1);

namespace ERP\Controllers;

use ERP\DTO\CompanyDTO;
use ERP\Forms\CompanyForm;
use ERP\Services\CompanyService;
use Phalcon\Http\Response;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;

/**
 * @property CompanyService $companyService
 */
class CompanyController extends Controller
{
    public function createAction(): ?ResponseInterface
    {
        try {
            $companyDTO = new CompanyDTO();
            $formCompany = new CompanyForm($companyDTO);

            $this->view->form = $formCompany;

            if ($this->request->isPost()) {
                $formCompany->bind($this->request->getPost(), $companyDTO);

                if ($formCompany->isValid()) {
                    $newId = $this->companyService->saveCompany($companyDTO);

                    $this->flashSession->success('Company with ID ' . $newId . ' has been created');

                    return $this->response->redirect('/company/list');
                }
            }
        } catch (\Throwable $e) {
            $this->flashSession->error($e->getMessage());
        }

        return null;
    }

    public function updateAction(int $id): ?ResponseInterface
    {
        try {
            $companyDTO = $this->companyService->initCompanyDTOById($id);

            $formCompany = new CompanyForm($companyDTO);

            $this->view->form = $formCompany;
            $this->view->companyId = $id;

            if ($this->request->isPost()) {
                $formCompany->bind($this->request->getPost(), $companyDTO);

                if ($formCompany->isValid()) {
                    $newId = $this->companyService->saveCompany($companyDTO);

                    $this->flashSession->success('Company with ID ' . $newId . ' has been updated');

                    return $this->response->redirect('/company/list');
                }
            }
        } catch (\Throwable $e) {
            if ($e->getCode() === 404) {
                return new Response($e->getMessage(), 404);
            }

            $this->flashSession->error($e->getMessage());
        }

        return null;
    }

    public function deleteAction(int $id): ?ResponseInterface
    {
        try {
            $this->companyService->deleteCompanyById($id);

            $this->flashSession->success('Company with ID ' . $id . ' has been deleted');
        } catch (\Throwable $e) {
            if ($e->getCode() === 404) {
                return new Response($e->getMessage(), 404);
            }

            $this->flashSession->error($e->getMessage());
        }

        return $this->response->redirect('/company/list');
    }

    public function viewAction(int $id): ?ResponseInterface
    {
        try {
            $companyDTO = $this->companyService->initCompanyDTOById($id);
            $this->view->companyDTO = $companyDTO;
        } catch (\Throwable $e) {
            $this->flashSession->error($e->getMessage());
        }

        return null;
    }

    public function listAction(): ?ResponseInterface
    {
        try {
            $companies = $this->companyService->listCompanies();
            $this->view->companies = $companies;
        } catch (\Throwable $e) {
            $this->flashSession->error($e->getMessage());
        }

        return null;
    }
}

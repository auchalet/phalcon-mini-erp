<?php

declare(strict_types=1);

namespace ERP\Forms;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;

class CompanyForm extends Form
{
    public function initialize(): void
    {
        $this
            ->add(
                new Text('name', [
                    'class' => 'form-control'
                ])
            )
            ->add(
                new Text('country', [
                    'class' => 'form-control'
                ])
            );
    }

    public function getCsrf(): string
    {
        return $this->security->getToken();
    }
}

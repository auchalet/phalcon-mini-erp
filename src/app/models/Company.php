<?php

declare(strict_types=1);

namespace ERP\Models;

use Phalcon\Mvc\Model;

class Company extends Model
{
    public ?int $id = null;

    public string $name;

    public ?float $balance = 0;

    public string $country;
}

<?php

declare(strict_types=1);

namespace ERP\Repositories;

use ERP\Models\Company;
use Phalcon\Mvc\ModelInterface;

class CompanyRepository
{
    public function findById(int $id): ?ModelInterface
    {
        return Company::findFirst('id = ' . $id);
    }

    public function findAll(): array
    {
        return Company::find()->toArray();
    }
}

<?php

declare(strict_types=1);

namespace ERP\Services;

use ERP\DTO\CompanyDTO;
use ERP\Models\Company;
use ERP\Repositories\CompanyRepository;

class CompanyService
{
    protected CompanyRepository $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function initCompanyFromDTO(CompanyDTO $companyDTO): Company
    {
        $company = new Company();
        $company->name = $companyDTO->name;
        $company->country = $companyDTO->country;
        $company->balance = 0;

        return $company;
    }

    public function saveCompany(CompanyDTO $companyDTO): int
    {
        if ($companyDTO->id) {
            $company = $this->companyRepository->findById($companyDTO->id);
        } else {
            $company = new Company();
        }

        $company->name = $companyDTO->name;
        $company->country = $companyDTO->country;

        if ($companyDTO->id === null) {
            $company->balance = 0;
        }

        $company->save();

        return $company->id;
    }

    public function initCompanyDTOById(int $id): CompanyDTO
    {
        /** @var Company|null $company */
        $company = $this->companyRepository->findById($id);

        if (null === $company) {
            throw new \Exception('Company not found', 404);
        }

        return $this->initCompanyDTOByCompany($company);
    }

    public function deleteCompanyById(int $id): bool
    {
        /** @var Company|null $company */
        $company = $this->companyRepository->findById($id);

        if (null === $company) {
            throw new \Exception('Company not found', 404);
        }

        return $company->delete();
    }

    public function listCompanies(): array
    {
        $companies = [];

        $companiesDB = $this->companyRepository->findAll();

        foreach ($companiesDB as $companyDB) {
            // Strange behaviour -- Sometimes return array data
            if (is_array($companyDB)) {
                $companies[] = $this->initCompanyDTOByCompanyData($companyDB);
            } else {
                $companies[] = $this->initCompanyDTOByCompany($companyDB);
            }
        }

        return $companies;
    }

    protected function initCompanyDTOByCompany(Company $company): CompanyDTO
    {
        $companyDTO = new CompanyDTO();
        $companyDTO->id = $company->id;
        $companyDTO->name = $company->name;
        $companyDTO->country = $company->country;
        $companyDTO->balance = $company->balance;

        return $companyDTO;
    }

    protected function initCompanyDTOByCompanyData(array $companyData): CompanyDTO
    {
        $companyDTO = new CompanyDTO();
        $companyDTO->id = (int) $companyData['id'];
        $companyDTO->name = $companyData['name'];
        $companyDTO->country = $companyData['country'];
        $companyDTO->balance = (float) $companyData['balance'];

        return $companyDTO;
    }
}

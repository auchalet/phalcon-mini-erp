<form method='post'>

    <div class="mb-3">
        <label for="formFile" class="form-label">Name</label>
        {{ form.render('name') }}
    </div>

    <p>
        <label>
            Country
        </label>

        {{ form.render('country') }}
    </p>

    <p>
        <input type="submit" class="btn btn-primary" value="Save new company" />
    </p>

</form>
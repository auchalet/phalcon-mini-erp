{% extends 'main.volt' %}

{% block body %}

    <h1>Create Company</h1>

    <hr>

    {% include 'company/_form.volt' %}

{% endblock %}
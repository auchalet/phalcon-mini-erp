{% extends 'main.volt' %}

{% block body %}

    <h1>List companies</h1>

    <hr>

    <a href="{{ url('company/create') }}" class="btn btn-primary">New company</a>

    <hr>

    {% if companies|length %}

    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Country</th>
            <th scope="col">Balance</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>

        {% for company in companies %}
        <tr>
            <th scope="row">{{ company.id }}</th>
            <td>{{ company.name }}</td>
            <td>{{ company.country }}</td>
            <td>{{ company.balance }}</td>
            <td>
                <a href="{{ url('company/view/' ~ company.id) }}">View</a>
                <a href="{{ url('company/update/' ~ company.id) }}">Update</a>
                <a href="{{ url('company/delete/' ~ company.id) }}">Delete</a>
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>

    {% else %}

        No records for Company

    {% endif %}

{% endblock %}
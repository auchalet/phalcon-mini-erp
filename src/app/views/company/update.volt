{% extends 'main.volt' %}

{% block body %}

    <h1>Update Company - ID {{ companyId }}</h1>

    <hr>

    {% include 'company/_form.volt' %}

{% endblock %}
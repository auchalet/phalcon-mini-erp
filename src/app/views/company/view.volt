{% extends 'main.volt' %}

{% block body %}

    <h1>View Company - ID {{ companyDTO.id }}</h1>

    <hr>

    <ul>
        <li>Name : {{ companyDTO.name }}</li>
        <li>Country : {{ companyDTO.country }}</li>
        <li>Balance : {{ companyDTO.balance }}</li>
    </ul>

{% endblock %}
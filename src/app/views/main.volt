<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{% block title %}{% endblock %}</title>

    <link href="https://fonts.googleapis.com/css2?family = Montserrat: wght @ 700&family=Roboto+Condensed:ital,wght@0,400;0,700;1,400;1,700&family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap"
          rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="css/main.css">

    {% block stylesheets %}{% endblock %}
</head>
<body>
        {% include 'layouts/header.volt' %}

        <div class="container-fluid">
            <div class="row">
                {% include 'layouts/sidebar.volt' %}

                <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 pt-3">
                        {{ content() }}
                        {{ flashSession.output() }}

                        {% block body %}{% endblock %}
                </div>

            </div>
        </div>


</main>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

{% block javascripts %}{% endblock %}
</body>
</html>

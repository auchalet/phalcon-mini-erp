<?php

declare(strict_types=1);

namespace ERP\Tests\Unit;

use Codeception\Test\Unit;
use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use PHPUnit\Framework\IncompleteTestError;
use UnitTester;

class AbstractTest extends Unit
{
    private bool $loaded = false;

    protected UnitTester $tester;

    protected function setUp(): void
    {
        parent::setUp();

        $di = new FactoryDefault();

        Di::reset();
        Di::setDefault($di);

        // Load db module
        $dbModule = $this->getModule('Phalcon4')->grabServiceFromContainer('db');
        $di->setShared('db', function () use ($dbModule) {
            return $dbModule;
        });

        $this->loaded = true;
    }

    public function __destruct()
    {
        if (!$this->loaded) {
            throw new IncompleteTestError(
                "Please run parent::setUp()."
            );
        }
    }
}

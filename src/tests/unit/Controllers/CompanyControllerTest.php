<?php

declare(strict_types=1);

namespace ERP\Tests\Controllers;

use Codeception\Lib\Connector\Phalcon4\SessionManager;
use ERP\DTO\CompanyDTO;
use ERP\Repositories\CompanyRepository;
use ERP\Services\CompanyService;
use ERP\Tests\Unit\AbstractTest;
use ERP\Controllers\CompanyController;
use Phalcon\Flash\Session;
use Phalcon\Http\Request;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\View;

class CompanyControllerTest extends AbstractTest
{
    protected CompanyController $controller;

    public function testCreateAction(): void
    {
        $controller = new CompanyController();

        // Unload services return \Throwable
        $this->tester->expectThrowable(\Throwable::class, function () use ($controller) {
            $controller->createAction();
        });

        // Mock services
        $viewMock = $this->getMockBuilder(View::class)->getMock();
        $controller->view = $viewMock;

        $flashSessionMock = $this->getMockBuilder(Session::class)->getMock();
        $controller->flashSession = $flashSessionMock;

        // Get request return void
        $this->assertEquals(null, $controller->createAction());


        // Prepare POST
        $_POST = [
            'name' => 'TestName',
            'country' => 'France',
        ];

        // Mock services used on POST
        $requestMock = $this->getMockBuilder(Request::class)
            ->onlyMethods(['isPost'])
            ->getMock();

        $companyServiceMock = $this->getMockBuilder(CompanyService::class)
            ->setConstructorArgs([new CompanyRepository()])
            ->onlyMethods(['saveCompany'])
            ->getMock();

        $requestMock->expects($this->once())
            ->method('isPost')
            ->will($this->returnValue(true));

        $controller->request = $requestMock;

        $companyServiceMock->expects($this->once())
            ->method('saveCompany')
            ->will($this->returnValue(1));

        $controller->companyService = $companyServiceMock;

        // Post request return ResponseInterface
        $this->assertInstanceOf(ResponseInterface::class, $controller->createAction());
    }

    public function testUpdateAction(): void
    {
        $controller = new CompanyController();

        // Unload services return \Throwable
        $this->tester->expectThrowable(\Throwable::class, function () use ($controller) {
            $controller->updateAction(1);
        });

        // Mock services
        $viewMock = $this->getMockBuilder(View::class)->getMock();
        $controller->view = $viewMock;

        $flashSessionMock = $this->getMockBuilder(Session::class)->getMock();
        $controller->flashSession = $flashSessionMock;

        $companyServiceMock = $this->getMockBuilder(CompanyService::class)
            ->setConstructorArgs([new CompanyRepository()])
            ->onlyMethods(['initCompanyDTOById', 'saveCompany'])
            ->getMock();

        $companyDTO = new CompanyDTO();
        $companyDTO->id = 1;
        $companyDTO->name = 'TestCompany';
        $companyDTO->country = 'France';

        $companyServiceMock
            ->expects($this->exactly(3))
            ->method('initCompanyDTOById')
            ->withConsecutive([999], [1], [1])
            ->willReturnOnConsecutiveCalls(
                $this->throwException(new \Exception('Company not found', 404)),
                $this->returnValue($companyDTO),
                $this->returnValue($companyDTO)
            );

        $controller->companyService = $companyServiceMock;


        // Get request with no record for ID return Reponse with 404 status
        $response = $controller->updateAction(999);
        $this->assertEquals(404, $response->getStatusCode());


        // Get request return void
        $this->assertEquals(null, $controller->updateAction(1));


        // Prepare POST
        $_POST = [
            'name' => 'TestUpdate',
            'country' => 'France',
        ];

        // Mock services used on POST
        $requestMock = $this->getMockBuilder(Request::class)
            ->onlyMethods(['isPost'])
            ->getMock();

        $requestMock->expects($this->once())
            ->method('isPost')
            ->will($this->returnValue(true));

        $controller->request = $requestMock;

        // Post request return ResponseInterface
        $this->assertInstanceOf(ResponseInterface::class, $controller->updateAction(1));
    }

    public function testDeleteAction(): void
    {
        $controller = new CompanyController();

        // Unload services return \Throwable
        $this->tester->expectThrowable(\Throwable::class, function () use ($controller) {
            $controller->deleteAction(1);
        });

        // Mock services
        $flashSessionMock = $this->getMockBuilder(Session::class)->getMock();
        $controller->flashSession = $flashSessionMock;

        $companyServiceMock = $this->getMockBuilder(CompanyService::class)
            ->setConstructorArgs([new CompanyRepository()])
            ->onlyMethods(['deleteCompanyById'])
            ->getMock();

        $companyServiceMock
            ->expects($this->exactly(2))
            ->method('deleteCompanyById')
            ->withConsecutive([999], [1])
            ->willReturnOnConsecutiveCalls(
                $this->throwException(new \Exception('Company not found', 404)),
                $this->returnValue(true),
            );

        $controller->companyService = $companyServiceMock;

        $response = $controller->deleteAction(999);
        $this->assertEquals(404, $response->getStatusCode());

        $this->assertInstanceOf(ResponseInterface::class, $controller->deleteAction(1));
    }

    public function testViewAction(): void
    {
        $controller = new CompanyController();

        // Unload services return \Throwable
        $this->tester->expectThrowable(\Throwable::class, function () use ($controller) {
            $controller->viewAction(1);
        });

        // Mock services
        $viewMock = $this->getMockBuilder(View::class)->getMock();
        $controller->view = $viewMock;

        $flashSessionMock = $this->getMockBuilder(Session::class)->getMock();
        $controller->flashSession = $flashSessionMock;

        $companyDTO = new CompanyDTO();
        $companyDTO->id = 1;
        $companyDTO->name = 'TestCompany';
        $companyDTO->country = 'France';

        $companyServiceMock = $this->getMockBuilder(CompanyService::class)
            ->setConstructorArgs([new CompanyRepository()])
            ->onlyMethods(['initCompanyDTOById'])
            ->getMock();

        $companyServiceMock
            ->expects($this->exactly(2))
            ->method('initCompanyDTOById')
            ->withConsecutive([999], [1])
            ->willReturnOnConsecutiveCalls(
                $this->throwException(new \Exception('Company not found', 404)),
                $this->returnValue($companyDTO),
            );

        $controller->companyService = $companyServiceMock;

        $response = $controller->updateAction(999);
        $this->assertEquals(404, $response->getStatusCode());

        $this->assertEquals(null, $controller->viewAction(1));
    }

    public function testListAction(): void
    {
        $controller = new CompanyController();

        // Unload services return \Throwable
        $this->tester->expectThrowable(\Throwable::class, function () use ($controller) {
            $controller->listAction();
        });

        $viewMock = $this->getMockBuilder(View::class)->getMock();
        $controller->view = $viewMock;

        $flashSessionMock = $this->getMockBuilder(Session::class)->getMock();
        $controller->flashSession = $flashSessionMock;

        $companyDTO1 = new CompanyDTO();
        $companyDTO1->id = 1;
        $companyDTO1->name = 'TestCompany';
        $companyDTO1->country = 'France';

        $companyDTO2 = new CompanyDTO();
        $companyDTO2->id = 2;
        $companyDTO2->name = 'TestCompany2';
        $companyDTO2->country = 'France';

        $result = [$companyDTO1, $companyDTO2];

        $companyServiceMock = $this->getMockBuilder(CompanyService::class)
            ->setConstructorArgs([new CompanyRepository()])
            ->onlyMethods(['listCompanies'])
            ->getMock();

        $companyServiceMock->expects($this->once())
            ->method('listCompanies')
            ->will($this->returnValue($result));

        $controller->companyService = $companyServiceMock;

        $this->assertEquals(null, $controller->listAction());
    }
}

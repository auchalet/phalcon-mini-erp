<?php

declare(strict_types=1);

use ERP\Forms\CompanyForm;
use ERP\Tests\Unit\AbstractTest;

class CompanyFormTest extends AbstractTest
{
    protected CompanyForm $companyForm;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testInitialize(): void
    {
        $companyForm = new CompanyForm();

        $this->assertCount(2, $companyForm->getElements());
        $this->assertEquals(true, array_key_exists('name', $companyForm->getElements()));
        $this->assertEquals(true, array_key_exists('country', $companyForm->getElements()));
    }

    public function testGetCsrf(): void
    {
        $companyForm = new CompanyForm();

        $this->assertIsString($companyForm->getCsrf());
    }
}

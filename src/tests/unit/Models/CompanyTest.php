<?php

declare(strict_types=1);

namespace ERP\Tests\Unit\Models;

use ERP\Models\Company;
use ERP\Tests\Unit\AbstractTest;
use Phalcon\Mvc\Model;
use UnitTester;

class CompanyTest extends AbstractTest
{
    protected Company $company;

    protected function setUp(): void
    {
        parent::setUp();

        $this->company = new Company();
    }

    public function testIsModel(): void
    {
        $this->tester->assertInstanceOf(Model::class, $this->company);
    }

    public function testNameIsString(): void
    {
        $this->tester->expectThrowable(\TypeError::class, function () {
            $this->company->name = 123;
        });

        $this->tester->expectThrowable(Model\Exception::class, function () {
            $this->company->name = true;
        });
    }

    public function testCountryIsString(): void
    {
        $this->tester->expectThrowable(\TypeError::class, function () {
            $this->company->country = 123;
        });

        $this->tester->expectThrowable(Model\Exception::class, function () {
            $this->company->country = true;
        });
    }
}

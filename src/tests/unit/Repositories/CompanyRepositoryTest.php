<?php

declare(strict_types=1);

namespace ERP\Tests\Repositories;

use ERP\Repositories\CompanyRepository;
use ERP\Tests\Unit\AbstractTest;

class CompanyRepositoryTest extends AbstractTest
{
    public function testFindById(): void
    {
        $repository = new CompanyRepository();

        // Bad call throw \Throwable
        $this->tester->expectThrowable(\Throwable::class, function () use ($repository) {
            $repository->findById('test');
        });

        $this->assertEquals(null, $repository->findById(1));
    }

    public function testFindAll(): void
    {
        $repository = new CompanyRepository();
        $this->assertIsArray($repository->findAll());
    }
}

<?php

declare(strict_types=1);

namespace ERP\Tests\Unit;

use ERP\DTO\CompanyDTO;
use ERP\Models\Company;
use ERP\Repositories\CompanyRepository;
use ERP\Services\CompanyService;
use ERP\Tests\Unit\AbstractTest;

class CompanyServiceTest extends AbstractTest
{
    protected CompanyService $companyService;

    public function testInitCompanyFromDTO(): void
    {
        $companyService = new CompanyService(new CompanyRepository());

        $companyDTO = $this->initCompanyDTO();
        $this->assertInstanceOf(Company::class, $companyService->initCompanyFromDTO($companyDTO));

        // Bad parameter type
        $object = new class () {};
        $this->tester->expectThrowable(\TypeError::class, function () use ($object, $companyService) {
            $companyService->initCompanyFromDTO($object);
        });
    }

    public function testSaveCompany(): void
    {
        $companyRepositoryMock = $this->getMockBuilder(CompanyRepository::class)
            ->onlyMethods(['findById'])
            ->getMock();

        $company = new Company();
        $company->id = 1;
        $company->name = 'TestCompany';
        $company->country = 'France';

        $companyRepositoryMock->expects($this->once())
            ->method('findById')
            ->will($this->returnValue($company));

        $companyService = new CompanyService($companyRepositoryMock);

        $companyDTO = $this->initCompanyDTO(1);
        $this->assertIsInt($companyService->saveCompany($companyDTO));

        $companyDTO = $this->initCompanyDTO();
        $this->assertIsInt($companyService->saveCompany($companyDTO));

        // Bad parameter type
        $object = new class () {};
        $this->tester->expectThrowable(\TypeError::class, function () use ($object, $companyService) {
            $companyService->saveCompany($object);
        });
    }

    public function testInitCompanyDTOById(): void
    {
        // Mock repository
        $companyRepositoryMock = $this->getMockBuilder(CompanyRepository::class)
            ->onlyMethods(['findById'])
            ->getMock();

        $company = new Company();
        $company->id = 1;
        $company->name = 'TestCompany';
        $company->country = 'France';

        $companyRepositoryMock->expects($this->exactly(2))
            ->method('findById')
            ->withConsecutive([999], [1])
            ->willReturnOnConsecutiveCalls(
                $this->returnValue(null),
                $this->returnValue($company)
            );


        $companyService = new CompanyService($companyRepositoryMock);

        // Company not found
        $this->tester->expectThrowable(\Exception::class, function () use ($companyService) {
            $companyService->initCompanyDTOById(999);
        });

        // Company found
        $this->assertInstanceOf(CompanyDTO::class, $companyService->initCompanyDTOById(1));
    }

    public function testDeleteCompanyById(): void
    {
        $companyRepositoryMock = $this->getMockBuilder(CompanyRepository::class)
            ->onlyMethods(['findById'])
            ->getMock();

        $company = new Company();
        $company->id = 1;
        $company->name = 'TestCompany';
        $company->country = 'France';

        $companyRepositoryMock
            ->expects($this->exactly(2))
            ->method('findById')
            ->withConsecutive([999], [1])
            ->willReturnOnConsecutiveCalls(
                $this->returnValue(null),
                $this->returnValue($company),
            );

        $companyService = new CompanyService($companyRepositoryMock);

        $this->tester->expectThrowable(\Exception::class, function () use ($companyService) {
            $companyService->deleteCompanyById(999);
        });

        $this->assertEquals(true, $companyService->deleteCompanyById($company->id));
    }

    public function testListCompanies(): void
    {
        $companyRepositoryMock = $this->getMockBuilder(CompanyRepository::class)
            ->onlyMethods(['findAll'])
            ->getMock();

        $company1 = new Company();
        $company1->id = 1;
        $company1->name = 'TestCompany';
        $company1->country = 'France';

        $company2 = new Company();
        $company2->id = 2;
        $company2->name = 'TestCompany2';
        $company2->country = 'France';

        $result = [$company1, $company2];

        // Test for behaviour Company or Array returned by Repository
        $companyRepositoryMock
            ->expects($this->exactly(2))
            ->method('findAll')
            ->withConsecutive([], [])
            ->willReturnOnConsecutiveCalls(
                $this->returnValue($result),
                $this->returnValue([$company1->toArray(), $company2->toArray()]),
            );

        $companyService = new CompanyService($companyRepositoryMock);

        $this->assertCount(2, $companyService->listCompanies());
        $this->assertCount(2, $companyService->listCompanies());
    }

    protected function initCompanyDTO(?int $id = null): CompanyDTO
    {
        $companyDTO = new CompanyDTO();

        if ($id) {
            $companyDTO->id = $id;
        }

        $companyDTO->name = 'TestCompany';
        $companyDTO->country = 'France';

        return $companyDTO;
    }
}
